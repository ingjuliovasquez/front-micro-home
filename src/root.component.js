import BlogCard from "./components/BlogCard";
import CardService from "./components/CardService";
import { Button, Input } from 'antd'

export default function Root(props) {
  return <main className="bg-gray-100 h-screen" >
    <section className="container mx-auto pt-20" >
      <div className="flex w-full gap-5">
        <div className="w-1/2">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend sodales ante, id
            condimentum orci pellentesque id. In pharetra ullamcorper diam ac iaculis. Phasellus non nisl
            dapibus, facilisis nunc non, dapibus turpis. In nec nunc id erat faucibus imperdiet. Vestibulum
            semper vulputate turpis, posuere posuere dolor hendrerit non. Sed varius mi a efficitur vulputate.
            Etiam at nibh placerat urna efficitur vulputate eu sed quam. Integer in nisi nisl. Interdum et
            malesuada fames ac ante ipsum primis in faucibus. Integer dapibus eu nisl vel semper. Cras et
            pretium libero, quis pulvinar justo. In vitae pretium mi. Aliquam in pharetra felis. Ut eu
            erat id arcu tincidunt mattis at eu libero. Proin interdum justo elit, bibendum dapibus sapien
            rutrum nec. Nunc dictum dolor massa, sit amet lacinia sem fermentum nec.
          </p>
        </div>
        <div className="w-1/2 flex flex-col gap-5 ">
          <Input placeholder="texto 1" />
          <Input placeholder="texto 2" />
          <Button type="primary" href="#" className="w-fit" >
            Botón
          </Button>
        </div>
      </div>
    </section>
  </main>
}
