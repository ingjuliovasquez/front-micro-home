import { Button } from 'antd'

export default function CardService({ title, text, img }) {
    return <div class="md:w-1/2 md:px-4 lg:w-1/4">
        <div class="bg-white rounded-lg border border-gray-300 p-8 h-full">
            <img src={img} alt="" class="h-20 mx-auto" />

            <h4 class="text-xl font-bold mt-4">{title}</h4>
            <p class="mt-1"></p>
            <Button  >
                Ver más...
            </Button>
        </div>
    </div>

}
