import React from 'react';
import { Card } from 'antd';

export default function BlogCard() {
    return (
        <div class="md:px-4 md:w-1/2 xl:w-1/3">
            <Card class="bg-white rounded border border-gray-300">
                <div class="w-full h-48 overflow-hidden bg-gray-300"></div>
                <div class="p-4">
                    <div class="flex items-center text-sm">
                        <span class="text-teal-500 font-semibold">Dental care</span>
                        <span class="ml-4 text-gray-600">29 Nov, 2023</span>
                    </div>
                    <p class="text-lg font-semibold leading-tight mt-4">Título</p>
                    <p class="text-gray-600 mt-1">
                        Este texto es un breve resumen del contenido del post original
                    </p>
                    <div class="flex items-center mt-4">
                        <div class="w-8 h-8 rounded-full overflow-hidden bg-gray-300"></div>
                        <div class="ml-4">
                            <p class="text-gray-600">Por <span class="text-gray-900 font-semibold">Julio Vásquez</span></p>
                        </div>
                    </div>
                </div>
            </Card>
        </div>
    )
}
